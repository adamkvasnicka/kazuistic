package com.kazu.kazu.Upload.service;

import com.cloudinary.Cloudinary;
import com.cloudinary.Singleton;
import com.cloudinary.Transformation;
import com.kazu.kazu.Upload.configuration.CloudinaryConfiguration;
import com.kazu.kazu.Upload.interfaces.InUploadConfiguration;
import lombok.Getter;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Getter
@Service
public class CloudinaryService implements InUploadConfiguration {

    private Cloudinary cloudinary;

    public CloudinaryService() {
        cloudinary = new Cloudinary();
        cloudinary = Singleton.getCloudinary();
        cloudinary.config.cloudName = CloudinaryConfiguration.cloudname;
        cloudinary.config.apiSecret = CloudinaryConfiguration.apisecret;
        cloudinary.config.apiKey = CloudinaryConfiguration.apikey;
    }

    public Map upload(Object file, Map options) {
        try {
            return cloudinary.uploader().upload(file, options);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String createUrl(String name, int width, int height, String action) {
        return cloudinary
                .url()
                .transformation(new Transformation().width(width).height(height)
                .border("2px_solid_black").crop(action))
                .imageTag(name);
    }

}
