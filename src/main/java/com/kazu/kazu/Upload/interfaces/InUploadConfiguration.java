package com.kazu.kazu.Upload.interfaces;

import java.util.Map;

public interface InUploadConfiguration {

    Map upload(Object file, Map options);

    String createUrl(String name, int width, int height, String action);

}
