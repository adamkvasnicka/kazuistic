package com.kazu.kazu.Upload.controller;

import com.cloudinary.utils.ObjectUtils;
import com.kazu.kazu.Upload.service.CloudinaryService;
import com.kazu.kazu.Upload.model.Image;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/rest/admin/upload")
public class ImageController {

    private CloudinaryService cloudinaryConfig;

    public ImageController() { cloudinaryConfig = new CloudinaryService(); }

    @CrossOrigin
    @PostMapping(value = "")
    public ResponseEntity postImage(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        } else {
            try {
                Map uploadResult = cloudinaryConfig.upload(
                        file.getBytes(),
                        ObjectUtils.asMap("resourcetype", "auto")
                );
                return ResponseEntity.ok(new Image(uploadResult.get("url").toString(), null));
            } catch (IOException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
            }
        }
    }

    @CrossOrigin
    @DeleteMapping(value = "/{id}")
    public ResponseEntity removeImage(@PathVariable final String id) {
        try {
            cloudinaryConfig.getCloudinary().uploader().destroy(id, ObjectUtils.emptyMap());
            return ResponseEntity.ok(null);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
    }

}
