package com.kazu.kazu.utilities;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public class GeneratorUtilities {

    @NotNull
    @Contract(pure = true)
    public static Integer addOptionalIntegerData(Integer optionalValue, Integer accumulator) {
        return Optional.ofNullable(optionalValue).orElse(0) + accumulator;
    }

}
