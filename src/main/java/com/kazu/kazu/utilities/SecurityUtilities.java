package com.kazu.kazu.utilities;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SecurityUtilities {

    private static final String charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";

    @NotNull
    public static String generateRandomPassword(int length) {
        List<Character> charCollection = charset.chars().mapToObj(i -> (char) i).collect(Collectors.toList());
        Collections.shuffle(charCollection);

        StringBuilder stringBuilder = new StringBuilder(length);
        Random random = new Random();
        IntStream.range(0, length).map(i -> random.nextInt(charCollection.size())).mapToObj(charCollection::get).forEach(stringBuilder::append);

        return stringBuilder.toString();
    }

}
