package com.kazu.kazu.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.kazu.kazu.utilities.GeneratorUtilities;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Patient {

    public Patient(@NotNull Template template) {
        this.templateId = template.getId();
        this.diagnosis = template.getDiagnosis();
        this.maxMalus = template.getMaxMalus();
        this.minBonus = template.getMinBonus();
        this.maxPrice = template.getMaxPrice();
        this.properties = template.getGroups().stream().map(Group::getGenerators).flatMap(List::stream).map(Property::new).collect(Collectors.toList());
        }

    @Id
    private String id;

    private String templateId;

    private String diagnosis;
    private Integer minBonus = 0;
    private Integer maxMalus = 0;
    private Integer maxPrice = 0;

    private List<Property> properties = Collections.emptyList();

    public Boolean getWasRight(ArrayList<Generator> exams, String studentDiagnosis) {
        Integer malus = exams.stream().map(Generator::getMalus).reduce(0, GeneratorUtilities::addOptionalIntegerData);
        Integer bonus = exams.stream().map(Generator::getBonus).reduce(0, GeneratorUtilities::addOptionalIntegerData);
        Integer price = exams.stream().map(Generator::getPrice).reduce(0, GeneratorUtilities::addOptionalIntegerData);
        return getMaxPrice() >= price && getMinBonus() <= bonus && getMaxMalus() >= malus && diagnosis.equals(studentDiagnosis);
    }

}
