package com.kazu.kazu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.ArrayList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Case {

    @Null
    private String patientId;

    @NotNull
    private String diagnosis;

    @NotNull
    private ArrayList<String> exams;

    @Null
    private Boolean wasRight;

}
