package com.kazu.kazu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Contract;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.IntStream;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Generator {

    @Id
    private String id;
    @Null
    private Integer order;
    @Null
    private String type;

    @NotNull
    private String title;

    @Null
    private Boolean exam;

    @Null
    private Integer max;
    @Null
    private Integer min;
    @Null
    private String unit;

    @Null
    private Integer malus;
    @Null
    private Integer bonus;
    @Null
    private Integer price;

    @Null
    private ArrayList<String> text;
    @Null
    private ArrayList<ImageGroup> imageGroup;

    ImageGroup getRandomImageGroup() {
        if(getHasImageGroup()) {
            return imageGroup.get(new Random().nextInt(imageGroup.size()));
        } else return null;
    }

    String getRandomText() {
        if(getHasText()) {
            return text.get(new Random().nextInt(text.size()));
        } else return null;
    }

    String getRandomMinMax() {
        if (getHasMinMax()) {
            int[] values = IntStream.range(min, max).toArray();
            return String.valueOf(values[new Random().nextInt(values.length)]);
        } else return null;
    }

    @org.jetbrains.annotations.NotNull
    @Contract(pure = true)
    private Boolean getHasMinMax() {
       return min != null && max != null;
    }

    @org.jetbrains.annotations.NotNull
    @Contract(pure = true)
    private Boolean getHasImageGroup() {
        return imageGroup != null;
    }

    @org.jetbrains.annotations.NotNull
    @Contract(pure = true)
    private Boolean getHasText() {
        return text != null;
    }

}
