package com.kazu.kazu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Group {

    @Id
    String id;

    @Null
    private String title;

    @Null
    private Integer order;
    @Null
    private Boolean isPartialExam;

    @NotNull
    List<Generator> generators;
}
