package com.kazu.kazu.model;

import com.kazu.kazu.Upload.model.Image;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class ImageGroup {

    String title;
    ArrayList<Image> images;

}
