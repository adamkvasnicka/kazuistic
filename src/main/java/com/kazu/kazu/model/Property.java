package com.kazu.kazu.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
class Property {

    Property(@NotNull Generator generator) {
        this.id = generator.getId();
        this.title = generator.getTitle();

        this.exam = generator.getExam();
        this.malus = generator.getMalus();
        this.bonus = generator.getBonus();
        this.unit = generator.getUnit();
        this.price = generator.getPrice();

        this.text = generator.getRandomMinMax();
        this.imageGroup = generator.getRandomImageGroup();
        if (this.text == null && this.imageGroup == null) {
            this.text = generator.getRandomText();
        }
    }

    @NotNull
    private String id;

    @NotNull
    private String title;

    private Boolean exam;

    private Integer malus;

    private Integer bonus;

    private String unit;

    private Integer price;

    @Null
    private String text;

    @Null
    private ImageGroup imageGroup;

}
