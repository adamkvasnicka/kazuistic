package com.kazu.kazu.model;

import com.kazu.kazu.utilities.SecurityUtilities;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Student extends User {

    private List<Case> cases;

    public void setRandomPassword(int length) {
        this.setPassword(SecurityUtilities.generateRandomPassword(length));
    }

}
