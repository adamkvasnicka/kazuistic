package com.kazu.kazu.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Template {

    @Id
    private String id;

    @NotNull
    private String diagnosis;

    // TODO: Will be NotNull after DB reload
    @Null
    private String title;

    @NotNull
    private int minBonus;

    @NotNull
    private int maxMalus;

    @NotNull
    private int maxPrice;

    @NotNull
    private List<Group> groups;

}
