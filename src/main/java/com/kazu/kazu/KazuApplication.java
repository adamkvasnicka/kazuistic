package com.kazu.kazu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KazuApplication {

	public static void main(String[] args) {
		SpringApplication.run(KazuApplication.class, args);
	}

}
