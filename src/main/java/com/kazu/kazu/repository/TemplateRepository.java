package com.kazu.kazu.repository;

import com.kazu.kazu.model.Template;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TemplateRepository extends MongoRepository<Template, String> {}
