package com.kazu.kazu.repository;

import com.kazu.kazu.model.Group;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupRepository extends MongoRepository<Group, String> {
}
