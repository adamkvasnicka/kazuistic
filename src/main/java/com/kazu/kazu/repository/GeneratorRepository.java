package com.kazu.kazu.repository;

import com.kazu.kazu.model.Generator;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GeneratorRepository extends MongoRepository<Generator, String> {}
