package com.kazu.kazu.repository;

import com.kazu.kazu.model.Diagnosis;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface DiagnosisRepository extends MongoRepository<Diagnosis, String> {
    Optional<Diagnosis> findByDefinition(String definition);
    Boolean existsByDefinition(String definition);
}
