package com.kazu.kazu.web.rest.controller;

import com.kazu.kazu.model.Role;
import com.kazu.kazu.model.User;
import com.kazu.kazu.repository.RoleRepository;
import com.kazu.kazu.services.MongoUserDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/rest/public/user")
public class UserController {

    private RoleRepository roleRepository;
    private MongoUserDetailsService userService;

    @CrossOrigin
    @PostMapping(value = "")
    public ResponseEntity createNewUser(@Valid @RequestBody User user) {
        User remoteUser = userService.findUserByUsername(user.getUsername());
        if (remoteUser != null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        } else {
            userService.saveUser(user);
        }
        return ResponseEntity.ok(user);
    }

    @CrossOrigin
    @PostMapping(value = "/login")
    public User loginUser(@Valid @RequestBody User user) {
        userService.loadUserByUsername(user.getUsername());
        return null;
    }

    @CrossOrigin
    @PostMapping(value = "/role")
    public Role setRole(@Valid @RequestBody Role role) {
        roleRepository.save(role);
        return role;
    }

}
