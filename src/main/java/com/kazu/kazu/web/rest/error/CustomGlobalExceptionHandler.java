package com.kazu.kazu.web.rest.error;

import com.kazu.kazu.web.rest.exceptions.DiagnosisNotFoundException;
import com.kazu.kazu.web.rest.exceptions.PatientNotFoundException;
import com.kazu.kazu.web.rest.exceptions.TemplateNotFoundException;
import com.kazu.kazu.web.rest.exceptions.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DiagnosisNotFoundException.class)
    public ResponseEntity<ApiError> customHandleDiagnosisNotFound(Exception ex, WebRequest request) {
        ApiError errors = new ApiError(LocalDateTime.now(), HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(TemplateNotFoundException.class)
    public ResponseEntity<ApiError> customHandleTemplateNotFound(Exception ex, WebRequest request) {
        ApiError errors = new ApiError(LocalDateTime.now(), HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PatientNotFoundException.class)
    public ResponseEntity<ApiError> customHandlePatientNotFound(Exception ex, WebRequest request) {
        ApiError errors = new ApiError(LocalDateTime.now(), HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ApiError> customHandleUserNotFound(Exception ex, WebRequest request) {
        ApiError errors = new ApiError(LocalDateTime.now(), HttpStatus.NOT_FOUND.value(), ex.getMessage());
        return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
    }

}
