package com.kazu.kazu.web.rest.exceptions;

public class PatientNotFoundException extends RuntimeException {

    public PatientNotFoundException(String id) {
        super("Patient with given id not found : " + id);
    }

}
