package com.kazu.kazu.web.rest.exceptions;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String id) {
        super("User with given id not found : " + id);
    }

}
