package com.kazu.kazu.web.rest.exceptions;

public class DiagnosisNotFoundException extends RuntimeException {

    public DiagnosisNotFoundException(String id) {
        super("Diagnosis with given id not found : " + id);
    }

}
