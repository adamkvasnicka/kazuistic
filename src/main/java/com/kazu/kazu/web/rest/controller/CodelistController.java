package com.kazu.kazu.web.rest.controller;

import com.kazu.kazu.model.Diagnosis;
import com.kazu.kazu.repository.DiagnosisRepository;
import com.kazu.kazu.web.rest.exceptions.DiagnosisNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/rest/admin/codelist")
public class CodelistController {

    private DiagnosisRepository diagnosisRepository;

    @CrossOrigin
    @GetMapping(value = "/diagnosis")
    public List<Diagnosis> getDiagnosisCodelist() {
        return diagnosisRepository.findAll();
    }

    @CrossOrigin
    @PostMapping(value = "/diagnosis")
    public ResponseEntity addDiagnosisToCodelist(@Valid @RequestBody Diagnosis diagnosis) {
        //TODO better solution?
        if (diagnosisRepository.existsByDefinition(diagnosis.getDefinition())) return ResponseEntity.badRequest().body("Diagnosis with given definition already exists: " + diagnosis.getDefinition());
        diagnosisRepository.save(diagnosis);
        return ResponseEntity.ok(diagnosis);
    }

    @CrossOrigin
    @PutMapping(value = "/diagnosis/{id}")
    public ResponseEntity updateDiagnosis(@Valid @RequestBody Diagnosis diagnosis, @PathVariable("id") String id) {
        diagnosisRepository.findById(id).orElseThrow(() -> new DiagnosisNotFoundException(id));
        diagnosis.setId(id);
        diagnosisRepository.save(diagnosis);
        return ResponseEntity.ok(diagnosis);
    }

}
