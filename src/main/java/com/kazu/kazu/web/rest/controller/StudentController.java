package com.kazu.kazu.web.rest.controller;

import com.kazu.kazu.model.*;
import com.kazu.kazu.repository.DiagnosisRepository;
import com.kazu.kazu.repository.GeneratorRepository;
import com.kazu.kazu.repository.PatientRepository;
import com.kazu.kazu.repository.TemplateRepository;
import com.kazu.kazu.web.rest.exceptions.DiagnosisNotFoundException;
import com.kazu.kazu.web.rest.exceptions.PatientNotFoundException;
import com.kazu.kazu.web.rest.exceptions.TemplateNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/rest/student")
public class StudentController {

    private TemplateRepository templateRepository;
    private PatientRepository patientRepository;
    private DiagnosisRepository diagnosisRepository;
    private GeneratorRepository generatorRepository;

    @CrossOrigin
    @GetMapping(value = "")
    public Patient getRandomPatientForLoggedUser(@RequestParam(value = "id", required = false) final String id) {
        if (id != null) {
            return templateRepository
                    .findById(id)
                    .map(repository -> {
                        Patient patient = new Patient(repository);
                        patientRepository.save(patient);
                        return patient;
                    })
                    .orElseThrow(() -> new TemplateNotFoundException(id));
        } else {
            int templatesCount = Math.toIntExact(templateRepository.count());
            Patient patient = new Patient(templateRepository.findAll().get(new Random().nextInt(templatesCount)));
            patientRepository.save(patient);
            return patient;
        }
    }

    @CrossOrigin
    @GetMapping(value = "/list")
    public List<Case> getStudentCases() {
        return Collections.emptyList();
    }

    @CrossOrigin
    @PostMapping(value = "/{id}")
    public Case saveStudentCase(@Valid @RequestBody Case studentCase, @PathVariable final String id) {
        return patientRepository
                .findById(id)
                .map(foundPatient -> diagnosisRepository
                        .findByDefinition(studentCase.getDiagnosis())
                        .map(foundDiagnosis -> {
                            studentCase.setPatientId(id);
                            ArrayList<Generator> generators = studentCase
                                    .getExams()
                                    .stream()
                                    .map(generatorRepository::findById)
                                    .map(Optional::get)
                                    .collect(Collectors.toCollection(ArrayList::new));
                            studentCase.setWasRight(
                                    foundPatient.getWasRight(
                                            generators,
                                            foundDiagnosis.getDefinition()
                                    )
                            );
                            // TODO: Find current user here and save to him
                            return studentCase;
                        })
                        .orElseThrow(() -> new DiagnosisNotFoundException(id)))
                .orElseThrow(() -> new PatientNotFoundException(id));
    }

    @CrossOrigin
    @GetMapping(value = "/diagnosis")
    public ArrayList<String> getDiagnosisCodelist() {
        return diagnosisRepository
                .findAll()
                .stream()
                .map(Diagnosis::getDefinition)
                .collect(Collectors.toCollection(ArrayList::new));
    }

}
