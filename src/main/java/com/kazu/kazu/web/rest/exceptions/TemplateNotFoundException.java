package com.kazu.kazu.web.rest.exceptions;

public class TemplateNotFoundException extends RuntimeException {

    public TemplateNotFoundException(String id) {
        super("Template with given id not found : " + id);
    }

}
