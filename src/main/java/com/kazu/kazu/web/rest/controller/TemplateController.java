package com.kazu.kazu.web.rest.controller;

import com.kazu.kazu.model.Template;
import com.kazu.kazu.repository.GeneratorRepository;
import com.kazu.kazu.repository.TemplateRepository;
import com.kazu.kazu.web.rest.exceptions.TemplateNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping(value = "/api/rest/admin/template")
public class TemplateController {

    private TemplateRepository templateRepository;
    private GeneratorRepository generatorRepository;

    @CrossOrigin
    @GetMapping(value = "/list")
    public List<Template> getAllGenerators() {
        return templateRepository.findAll();
    }

    @CrossOrigin
    @PostMapping(value = "")
    public Template createNewPatient(@Valid @RequestBody Template template) {
        template.getGroups().forEach(group -> {group.getGenerators().forEach(generatorRepository::save);});
        templateRepository.save(template);
        return template;
    }

    @CrossOrigin
    @GetMapping(value = "/{id}")
    public Template getTemplateById(@PathVariable("id") String id) {
        return templateRepository.findById(id).orElseThrow(() -> new TemplateNotFoundException(id));
    }

    @CrossOrigin
    @PutMapping(value = "/{id}")
    public Template updateTemplate(@Valid @RequestBody Template template, @PathVariable("id") String id) {
        templateRepository.findById(id).orElseThrow(() -> new TemplateNotFoundException(id));
        template.setId(id);
        templateRepository.save(template);
        return template;
    }

}
